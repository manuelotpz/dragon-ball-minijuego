// link de ideas:
// https://codepen.io/chznbaum/details/GWqpqj

// INSTALACIONES INICIALES
const express = require('express')
const cors = require('cors')

const bodyParser = require('body-parser')

const mysql = require('mysql')

const app = express()

//COSAS QUE USA LA APP

app.use(cors())

app.use(bodyParser.urlencoded({extended: true}))

app.use(bodyParser.json())

